<?php

namespace Al\FFTTBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Partie
 *
 * @ORM\Table(name="partie")
 * @ORM\Entity(repositoryClass="Al\FFTTBundle\Repository\PartieRepository")
 */
class Partie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="isVictoire", type="boolean")
     */
    private $isVictoire;

    /**
     * @var int
     *
     * @ORM\Column(name="journee", type="integer")
     */
    private $journee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var float
     *
     * @ORM\Column(name="pointsObtenus", type="float")
     */
    private $pointsObtenus;

    /**
     * @var float
     *
     * @ORM\Column(name="coefficent", type="float")
     */
    private $coefficent;


    /**
     * @var int
     *
     * @ORM\Column(name="adversaire_licence", type="string")
     */
    private $adversaireLicence;

    /**
     * @var boolean
     *
     * @ORM\Column(name="adversaire_is_homme", type="boolean")
     */
    private $adversaireIsHomme;

    /**
     * @var string
     *
     * @ORM\Column(name="adversaire_nom", type="string")
     */
    private $adversaireNom;

    /**
     * @var string
     *
     * @ORM\Column(name="adversaire_prenom", type="string")
     */
    private $adversairePrenom;

    /**
     * @var int
     *
     * @ORM\Column(name="adversaire_classement", type="integer")
     */
    private $adversaireClassement;

    /**
     * @ORM\ManyToOne(targetEntity="Al\FFTTBundle\Entity\Joueur", inversedBy="parties")
     * @ORM\JoinColumn(nullable=false)
     */
    private $joueur;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isVictoire
     *
     * @param boolean $isVictoire
     *
     * @return Partie
     */
    public function setIsVictoire(bool $isVictoire)
    {
        $this->isVictoire = $isVictoire;

        return $this;
    }

    /**
     * Get isVictoire
     *
     * @return bool
     */
    public function getIsVictoire() : bool
    {
        return $this->isVictoire;
    }

    /**
     * Set journee
     *
     * @param integer $journee
     *
     * @return Partie
     */
    public function setJournee(int $journee)
    {
        $this->journee = $journee;

        return $this;
    }

    /**
     * Get journee
     *
     * @return int
     */
    public function getJournee() : int
    {
        return $this->journee;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Partie
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() : \DateTime
    {
        return $this->date;
    }

    /**
     * Set pointsObtenus
     *
     * @param float $pointsObtenus
     *
     * @return Partie
     */
    public function setPointsObtenus(float $pointsObtenus)
    {
        $this->pointsObtenus = $pointsObtenus;

        return $this;
    }

    /**
     * Get pointsObtenus
     *
     * @return float
     */
    public function getPointsObtenus() : float
    {
        return $this->pointsObtenus;
    }

    /**
     * Set coefficent
     *
     * @param float $coefficent
     *
     * @return Partie
     */
    public function setCoefficent(float $coefficent)
    {
        $this->coefficent = $coefficent;

        return $this;
    }

    /**
     * Get coefficent
     *
     * @return float
     */
    public function getCoefficent() : float
    {
        return $this->coefficent;
    }

    public function getAdversaireLicence(): string
    {
        return $this->adversaireLicence;
    }

    public function setAdversaireLicence(string $adversaireLicence)
    {
        $this->adversaireLicence = $adversaireLicence;
    }

    public function isAdversaireIsHomme(): bool
    {
        return $this->adversaireIsHomme;
    }

    public function setAdversaireIsHomme(bool $adversaireIsHomme)
    {
        $this->adversaireIsHomme = $adversaireIsHomme;
    }

    public function getAdversaireNom(): string
    {
        return $this->adversaireNom;
    }

    public function getAdversairePrenom(): string
    {
        return $this->adversairePrenom;
    }

    public function setAdversaireNom(string $adversaireNom)
    {
        $this->adversaireNom = $adversaireNom;
    }

    public function setAdversairePrenom(string $adversairePrenom)
    {
        $this->adversairePrenom = $adversairePrenom;
    }

    public function getAdversaireClassement(): int
    {
        return $this->adversaireClassement;
    }

    public function setAdversaireClassement(int $adversaireClassement)
    {
        $this->adversaireClassement = $adversaireClassement;
    }

    public function getJoueur() : Joueur
    {
        return $this->joueur;
    }

    public function setJoueur(Joueur $joueur)
    {
        $this->joueur = $joueur;
    }
}

