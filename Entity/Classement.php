<?php

namespace Al\FFTTBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classement
 *
 * @ORM\Table(name="classement")
 * @ORM\Entity(repositoryClass="Al\FFTTBundle\Repository\ClassementRepository")
 */
class Classement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="points", type="float")
     */
    private $points;

    /**
     * @var float
     *
     * @ORM\Column(name="anciensPoints", type="float")
     */
    private $anciensPoints;
    /**
     * @var int
     *
     * @ORM\Column(name="classement", type="integer")
     */
    private $classement;

    /**
     * @var int
     *
     * @ORM\Column(name="rangRegional", type="integer")
     */
    private $rangRegional;

    /**
     * @var int
     *
     * @ORM\Column(name="rangNational", type="integer")
     */
    private $rangNational;

    /**
     * @var int
     *
     * @ORM\Column(name="rangDepartemental", type="integer")
     */
    private $rangDepartemental;

    /**
     * @var int
     *
     * @ORM\Column(name="pointsOfficiels", type="integer")
     */
    private $pointsOfficiels;

    /**
     * @var float
     *
     * @ORM\Column(name="pointsInitials", type="float")
     */
    private $pointsInitials;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Al\FFTTBundle\Entity\Joueur", inversedBy="classements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $joueur;

    public function __construct(
        \DateTime $date = null,
        float $points = null,
        float $anciensPoints = null,
        int $classement = null,
        int $rangNational = null,
        int $rangRegional = null,
        int $rangDepartemental = null,
        int $pointsOfficiels = null,
        float $pointsInitials = null
    )
    {
        $this->date = $date;
        $this->points = $points;
        $this->anciensPoints = $anciensPoints;
        $this->classement = $classement;
        $this->rangNational = $rangNational;
        $this->rangRegional = $rangRegional;
        $this->rangDepartemental = $rangDepartemental;
        $this->pointsOfficiels = $pointsOfficiels;
        $this->pointsInitials = $pointsInitials;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set points
     *
     * @param float $points
     *
     * @return Classement
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return float
     */
    public function getPoints()
    {
        return $this->points;
    }

    public function getAnciensPoints(): float
    {
        return $this->anciensPoints;
    }

    public function setAnciensPoints(float $anciensPoints)
    {
        $this->anciensPoints = $anciensPoints;
    }

    /**
     * Set classement
     *
     * @param integer $classement
     *
     * @return Classement
     */
    public function setClassement($classement)
    {
        $this->classement = $classement;

        return $this;
    }

    /**
     * Get classement
     *
     * @return int
     */
    public function getClassement()
    {
        return $this->classement;
    }

    /**
     * Set rangRegional
     *
     * @param integer $rangRegional
     *
     * @return Classement
     */
    public function setRangRegional($rangRegional)
    {
        $this->rangRegional = $rangRegional;

        return $this;
    }

    /**
     * Get rangRegional
     *
     * @return int
     */
    public function getRangRegional()
    {
        return $this->rangRegional;
    }

    /**
     * Set rangNational
     *
     * @param integer $rangNational
     *
     * @return Classement
     */
    public function setRangNational($rangNational)
    {
        $this->rangNational = $rangNational;

        return $this;
    }

    /**
     * Get rangNational
     *
     * @return int
     */
    public function getRangNational()
    {
        return $this->rangNational;
    }

    /**
     * Set rangDepartemental
     *
     * @param integer $rangDepartemental
     *
     * @return Classement
     */
    public function setRangDepartemental($rangDepartemental)
    {
        $this->rangDepartemental = $rangDepartemental;

        return $this;
    }

    /**
     * Get rangDepartemental
     *
     * @return int
     */
    public function getRangDepartemental()
    {
        return $this->rangDepartemental;
    }

    /**
     * Set pointsOfficiels
     *
     * @param integer $pointsOfficiels
     *
     * @return Classement
     */
    public function setPointsOfficiels($pointsOfficiels)
    {
        $this->pointsOfficiels = $pointsOfficiels;

        return $this;
    }

    /**
     * Get pointsOfficiels
     *
     * @return int
     */
    public function getPointsOfficiels()
    {
        return $this->pointsOfficiels;
    }

    /**
     * Set pointsInitials
     *
     * @param float $pointsInitials
     *
     * @return Classement
     */
    public function setPointsInitials($pointsInitials)
    {
        $this->pointsInitials = $pointsInitials;

        return $this;
    }

    /**
     * Get pointsInitials
     *
     * @return float
     */
    public function getPointsInitials()
    {
        return $this->pointsInitials;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Classement
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    public function getJoueur()
    {
        return $this->joueur;
    }

    public function setJoueur($joueur)
    {
        $this->joueur = $joueur;
    }
}

