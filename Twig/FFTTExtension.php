<?php
/**
 * Created by Antoine Lamirault.
 */

namespace Al\FFTTBundle\Twig;

use FFTTApi\FFTTApi;
use FFTTApi\Service\PointCalculator;
use Symfony\Component\HttpFoundation\RequestStack;

class FFTTExtension extends \Twig_Extension
{
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var FFTTApi
     */
    private $FFTTApi;
    /**
     * @var PointCalculator
     */
    private $pointCalculator;


    /**
     * FFTTExtension constructor.
     * @param RequestStack $requestStack
     * @param FFTTApi $FFTTApi
     * @param PointCalculator $pointCalculator
     */
    public function __construct(RequestStack $requestStack, FFTTApi $FFTTApi, PointCalculator $pointCalculator)
    {
        $this->requestStack = $requestStack;
        $this->FFTTApi = $FFTTApi;
        $this->pointCalculator = $pointCalculator;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                'formatPlayerName',
                [$this, 'formatPlayerName'],
                ['needs_environment' => true]
            ),
            new \Twig_SimpleFunction(
                'getPointsContreAdversaire',
                [$this, 'getPointsContreAdversaire'],
                [
                    'needs_environment' => true,
                    'is_safe' => ['html'],
                ]
            ),
        ];
    }

    public function formatPlayerName(\Twig_Environment $environment, string $nom, string $prenom)
    {
        $template = $environment->createTemplate("{{ nom | upper ~' '~ prenom | capitalize}}");
        return $template->render(
            [
                "nom" => $nom,
                "prenom" => $prenom,
            ]
        );
    }

    public function getPointsContreAdversaire(\Twig_Environment $environment, string $adversaireId)
    {
        $id = $this->requestStack->getCurrentRequest()->getSession()->get('player');
        $joueur = $this->FFTTApi->getJoueurDetailsByLicence($id);
        $adversaire = $this->FFTTApi->getJoueurDetailsByLicence($adversaireId);

        $pointsVictoire = $this->pointCalculator->getPointVictory($joueur->getPointsMensuel(), $adversaire->getPointsMensuel());
        $pointsDefaite = $this->pointCalculator->getPointDefeat($joueur->getPointsMensuel(), $adversaire->getPointsMensuel());

        return $environment->render(
            '@AlFFTT/adversaire/potentialPoints.html.twig',
            [
                "pointsVictoire" => $pointsVictoire,
                "pointsDefaite" => $pointsDefaite,
            ]
        );
    }
}