<?php

namespace Al\FFTTBundle\Controller;

use Al\FFTTBundle\Entity\Equipe;
use Al\FFTTBundle\Model\RencontreStats;
use Al\FFTTBundle\Service\CacheRetriever;
use FFTTApi\Exception\InvalidLienRencontre;
use FFTTApi\Model\Rencontre\Rencontre;
use FFTTApi\Model\Rencontre\RencontreDetails;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\HttpFoundation\Request;

class EquipeController extends Controller implements ReloadCache
{
    public function listAction()
    {
        $equipes = $this->getDoctrine()->getRepository(Equipe::class)->findAll();
        return $this->render('@AlFFTT/Equipes/list.html.twig',[
            'equipes' => $equipes
        ]);
    }

    /**
     * @ParamConverter("equipe")
     */
    public function detailsAction(Equipe $equipe){

        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('equipe.details')
            ->setRouteParameters([
                'id' => $equipe->getId(),
            ])
            ->setLabelParameters([
                '%name%' => $equipe->getLibelle(),
            ]);


        $classements = $this->get('fftt.api')->getClassementPouleByLienDivision($equipe->getLienDivision());
        $rencontres = $this->get('fftt.api')->getRencontrePouleByLienDivision($equipe->getLienDivision());
        $rencontresGrouped = [];

        foreach ($rencontres as $rencontre ) {
            foreach ($classements as $equipePoule){
                if($rencontre->getNomEquipeA() === $equipePoule->getNomEquipe()){
                    $rencontre->setClubEquipeA($equipePoule->getNumero());
                }
                if($rencontre->getNomEquipeB() === $equipePoule->getNomEquipe()){
                    $rencontre->setClubEquipeB($equipePoule->getNumero());
                }
            }
            $rencontresGrouped[$rencontre->getLibelle()][] = $rencontre;
        }
        return $this->render('@AlFFTT/Equipes/details.html.twig',[
            'equipe' => $equipe,
            'classements' => $classements,
            'rencontresGrouped' => $rencontresGrouped,
            'lienDivision' => $equipe->getLienDivision(),
        ]);
    }

    /**
     * @ParamConverter("equipe")
     */
    public function rencontreAction(Equipe $equipe, Request $request, CacheRetriever $cacheRetriever){
        $lien = $request->get('lien');
        $forceReload = $request->get("reload") == "true";

        $clubEquipeA = $request->get('clubEquipeA');
        $clubEquipeB = $request->get('clubEquipeB');

        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('equipe.details')
            ->setRouteParameters([
                'id' => $equipe->getId(),
            ])
            ->setLabelParameters([
                '%name%' => $equipe->getLibelle(),
            ]);

        try{
            $rencontre = $cacheRetriever->getRencontreDetails($lien, $clubEquipeA, $clubEquipeB, $forceReload);
            if(!$rencontre){
                throw new InvalidLienRencontre("GEN");
            }
        }
        catch (InvalidLienRencontre $e){
            $this->get('thormeier_breadcrumb.breadcrumb_provider')
                ->getBreadcrumbByRoute('rencontre.details')
                ->setLabelParameters([
                    '%name%' => "Rencontre non trouvée",
                ]);

            return $this->render('@AlFFTT/Equipes/rencontreNotFound.html.twig');
        }

        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('rencontre.details')
            ->setLabelParameters([
                '%name%' => $rencontre->getNomEquipeA()." - ".$rencontre->getNomEquipeB(),
            ]);

        /** @var RencontreDetails $rencontre */

        $rencontreStats = RencontreStats::createFromRencontreDetails($rencontre);

        return $this->render('@AlFFTT/Equipes/rencontre.html.twig',[
            'rencontre' => $rencontre,
            'stats' => $rencontreStats,
        ]);
    }
}
