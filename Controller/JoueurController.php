<?php

namespace Al\FFTTBundle\Controller;

use Al\FFTTBundle\Entity\Historique;
use Al\FFTTBundle\Entity\Joueur;
use Al\FFTTBundle\Service\CacheRetriever;
use Al\FFTTBundle\Service\HighchartBuilder;
use Al\FFTTBundle\Service\StatsBuilder;
use FFTTApi\Exception\JoueurNotFound;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class JoueurController extends Controller implements ReloadCache
{
    /**
     * @var HighchartBuilder
     */
    private $highchartBuilder;
    /**
     * @var StatsBuilder
     */
    private $statsBuilder;
    /**
     * @var CacheRetriever
     */
    private $cacheRetriever;


    /**
     * JoueurController constructor.
     * @param HighchartBuilder $highchartBuilder
     * @param StatsBuilder $statsBuilder
     * @param CacheRetriever $cacheRetriever
     */
    public function __construct(HighchartBuilder $highchartBuilder, StatsBuilder $statsBuilder, CacheRetriever $cacheRetriever)
    {

        $this->highchartBuilder = $highchartBuilder;
        $this->statsBuilder = $statsBuilder;
        $this->cacheRetriever = $cacheRetriever;
    }

    public function listJoueursAction()
    {
        $joueurs = $this->getDoctrine()->getRepository(Joueur::class)->findAll();
        return $this->render('@AlFFTT/Joueurs/list.html.twig', [
            'joueurs' => $joueurs,
        ]);
    }

    /**
     * @param Joueur $joueur
     * @param HighchartBuilder $highchartBuilder
     * @return \Symfony\Component\HttpFoundation\Response
     * @ParamConverter("joueur")
     */
    public function detailsJoueurAction(Joueur $joueur)
    {

        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('joueur.details')
            ->setRouteParameters([
                'id' => $joueur->getId(),
            ])
            ->setLabelParameters([
                '%name%' => $joueur->getPrenom() . " " . $joueur->getNom(),
            ]);

        return $this->render('@AlFFTT/Joueurs/details.html.twig', [
            'joueur' => $joueur,
            'chart' => $this->statsBuilder->getEvolutionByDay($joueur),
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @ParamConverter("joueur")
     */
    public function notValidatedMatchJoueurAction(Request $request)
    {
        $joueurLic = $request->get("id");
        $forceReload = $request->get("reload") == "true";

        $parties = $this->cacheRetriever->getNotValidatedMatchs($joueurLic, $forceReload);

        return $this->render("@AlFFTT/Joueurs/notValidatedMatch.html.twig", ["parties" => $parties]);
    }

    public function virtualPointsJoueurAction(Request $request)
    {
        $id = $request->get("id");
        $forceReload = $request->get("reload") == "true";

        $virtualPoints = $this->cacheRetriever->getVirtualPoints($id, $forceReload);

        return new Response(round($virtualPoints, 3));
    }

    /**
     * @param Joueur $joueur
     * @ParamConverter("joueur")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function historiqueJoueurAction(Joueur $joueur)
    {

        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('joueur.details')
            ->setRouteParameters([
                'id' => $joueur->getId(),
            ])
            ->setLabelParameters([
                '%name%' => $joueur->getPrenom() . " " . $joueur->getNom(),
            ]);

        $points = [];
        $xAxis = [];
        /** @var Historique $historique */
        foreach ($joueur->getHistoriques() as $historique) {
            $points[] = $historique->getPoints();
            $xAxis[] = sprintf(
                "%s - %s (%s)",
                $historique->getAnneeDebut(),
                $historique->getAnneeFin(),
                $historique->getPhase()
            );
        }

        $chart = $this->highchartBuilder->buildHisto('container', $xAxis, $points);

        return $this->render('@AlFFTT/Joueurs/historique.html.twig', [
            'joueur' => $joueur,
            'chart' => $chart,
        ]);
    }
}
