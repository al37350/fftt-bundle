<?php

namespace Al\FFTTBundle\Controller;

use Al\FFTTBundle\Entity\Joueur;
use Al\FFTTBundle\Service\CacheRetriever;
use Al\FFTTBundle\Service\HighchartBuilder;
use Al\FFTTBundle\Service\StatsBuilder;
use FFTTApi\Exception\JoueurNotFound;
use FFTTApi\Exception\NoFFTTResponseException;
use FFTTApi\FFTTApi;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class AdversaireController extends Controller implements ReloadCache
{
    /**
     * @var CacheRetriever
     */
    private $cacheRetriever;
    /**
     * @var FFTTApi
     */
    private $api;

    /**
     * AdversaireController constructor.
     * @param CacheRetriever $cacheRetriever
     * @param FFTTApi $api
     */
    public function __construct(CacheRetriever $cacheRetriever, FFTTApi $api)
    {
        $this->cacheRetriever = $cacheRetriever;
        $this->api = $api;
    }

    /**
     * @ParamConverter("joueur")
     */
    public function joueurAction(Joueur $joueur, Request $request)
    {
        $id = $request->get('adversaire');
        $adversaire = $this->cacheRetriever->getJoueurDetailsByLicence($id, false);
        $parties = $this->api->getPartiesJoueurByLicence($id);

        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('joueur.details')
            ->setRouteParameters([
                'id' => $joueur->getId(),
            ])
            ->setLabelParameters([
                '%name%' => $joueur->getPrenom()." ".$joueur->getNom(),
            ]);

        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('joueur.adversaire')
            ->setLabelParameters([
                '%name%' => $adversaire->getPrenom()." ".$adversaire->getNom(),
            ]);

        return $this->render('@AlFFTT/adversaire/joueur.html.twig',[
            'joueur' => $adversaire,
            'parties' => $parties
        ]);
    }

    public function adversaireAction(Request $request, LoggerInterface $logger)
    {
        $id = $request->get('id');

        try{
            $adversaire = $this->cacheRetriever->getJoueurDetailsByLicence($id, false);
            $parties = $this->api->getPartiesJoueurByLicence($id);

            $this->get('thormeier_breadcrumb.breadcrumb_provider')
                ->getBreadcrumbByRoute('adversaire.details')
                ->setLabelParameters([
                    '%name%' => $adversaire->getPrenom()." ".$adversaire->getNom(),
                ]);

            return $this->render('@AlFFTT/adversaire/joueur.html.twig',[
                'joueur' => $adversaire,
                'parties' => $parties
            ]);
        }
        catch (JoueurNotFound $e){
            $this->get('thormeier_breadcrumb.breadcrumb_provider')
                ->getBreadcrumbByRoute('adversaire.details')
                ->setLabelParameters([
                    '%name%' => "non trouvé",
                ]);

            return $this->render('@AlFFTT/adversaire/joueurNotFound.html.twig', [
                'licence' => $id,
            ]);
        }
    }

    public function searchAction(Request $request)
    {
        $nom =  $request->get("nom");
        $prenom =  $request->get("prenom");

        $joueurs = [];
        try{
            $joueurs = $this->api->getJoueursByNom($nom, $prenom);
        }catch (NoFFTTResponseException $e){}

        if(count($joueurs) == 1){
            return $this->redirectToRoute("adversaire.details", [
                "id" => $joueurs[0]->getLicence()
            ]);
        }
        elseif (count($joueurs)){
            $this->addFlash("error", "Impossible de trouver ce joueur");
            return new RedirectResponse($this->generateUrl("adversaire.search"));
        }

        return $this->render('@AlFFTT/adversaire/chooseSearch.html.twig', [
            'joueurs' => $joueurs
        ]);
    }

    public function searchFormAction(Request $request){
        $form = $this->createFormBuilder()
            ->add("lastName", TextType::class,
                [
                    "label" => "Nom",
                    "required" => false,
                ]
            )->add("firstName", TextType::class,
                [
                    "label" => "Prénom",
                    "required" => false,
                ]
            )
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data =  $form->getData();
            try{
                $joueurs = $this->api->getJoueursByNom($data["lastName"] ?? "", $data["firstName"] ?? "");

                if(count($joueurs) == 1){
                    return $this->redirectToRoute("adversaire.details", [
                        "id" => $joueurs[0]->getLicence()
                    ]);
                }
                return $this->render('@AlFFTT/adversaire/chooseSearch.html.twig', [
                    'joueurs' => $joueurs
                ]);
            }
            catch (NoFFTTResponseException $e){
                $this->addFlash("error", "Aucun joueur de trouvé");
            }
        }

        return $this->render('@AlFFTT/adversaire/search.html.twig',
            [
                "form" => $form->createView()
            ]);
    }

    public function clubAction(Request $request){
        $id = $request->get('id');

        return $this->render('@AlFFTT/adversaire/club/club.html.twig', [
            "id" => $id
        ]);
    }

    public function statsAction(Request $request, StatsBuilder $statsBuilder, HighchartBuilder $highchartBuilder){
        $id = $request->get('id');
        $joueur = $this->cacheRetriever->getJoueurDetailsByLicence($id, false);

        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('adversaire.stats')
            ->setLabelParameters([
                '%name%' => $joueur->getPrenom()." ".$joueur->getNom(),
            ]);


        $matchStats = $statsBuilder->getStatMatchs($id);

        $repartition = $highchartBuilder->buildRepartition('repartition', $matchStats);

        return $this->render('@AlFFTT/adversaire/stats.html.twig',[
            'joueur' => $joueur,
            'matchStats' => $matchStats,
            "repartition" => $repartition,
        ]);
    }

    public function clubJoueursAction(Request $request){
        $id = $request->get('id');

        $joueurs = $this->api->getJoueursByClub($id);

        return $this->render('@AlFFTT/adversaire/club/joueurs.html.twig',[
            'joueurs' => $joueurs
        ]);
    }

    public function clubDetailsAction(Request $request){
        $id = $request->get('id');

        $club = $this->api->getClubDetails($id);

        return $this->render('@AlFFTT/adversaire/club/details.html.twig',[
            'club' => $club
        ]);
    }

    public function clubEquipesAction(Request $request){
        $id = $request->get('id');

        $equipes = $this->api->getEquipesByClub($id, 'M');

        return $this->render('@AlFFTT/adversaire/club/equipes.html.twig',[
            'equipes' => $equipes
        ]);
    }

}
