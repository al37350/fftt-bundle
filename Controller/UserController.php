<?php
/**
 * Created by PhpStorm.
 * User: alamirault
 * Date: 15/12/18
 * Time: 19:40
 */

namespace Al\FFTTBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends AbstractController
{

    public function joueurAsUserAction(Request $request){
        $request->getSession()->set("player", $request->get('id'));

        return new Response("");
    }

    public function unsetJoueurAsUserAction(Request $request){
        $request->getSession()->remove("player");

        return new Response("");
    }
}