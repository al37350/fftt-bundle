<?php

namespace Al\FFTTBundle\Controller;

use Al\FFTTBundle\EventSubscriber\CacheSubscriber;
use Al\FFTTBundle\Form\Type\ClubSearchType;
use Al\FFTTBundle\Model\RencontreStats;
use Al\FFTTBundle\Service\CacheRetriever;
use Al\FFTTBundle\Service\StatsBuilder;
use FFTTApi\Exception\InvalidLienRencontre;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\HttpFoundation\Request;

class ClubController extends Controller
{
    public function searchAction(Request $request)
    {
        $clubs = [];

        $form = $this->createForm(ClubSearchType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $clubs = $this->get('fftt.api')->getClubsByName($form->getData()["name"]);
        }

        return $this->render("@AlFFTT/club/search.html.twig", [
            "form" => $form->createView(),
            'clubs' => $clubs,
        ]);
    }

    public function equipeAction(string $lienDivision)
    {
        $classements = $this->get('fftt.api')->getClassementPouleByLienDivision($lienDivision);
        $rencontres = $this->get('fftt.api')->getRencontrePouleByLienDivision($lienDivision);
        $rencontresGrouped = [];

        foreach ($rencontres as $rencontre) {
            foreach ($classements as $equipePoule) {
                if ($rencontre->getNomEquipeA() === $equipePoule->getNomEquipe()) {
                    $rencontre->setClubEquipeA($equipePoule->getNumero());
                }
                if ($rencontre->getNomEquipeB() === $equipePoule->getNomEquipe()) {
                    $rencontre->setClubEquipeB($equipePoule->getNumero());
                }
            }
            $rencontresGrouped[$rencontre->getLibelle()][] = $rencontre;
        }
        return $this->render('@AlFFTT/club/equipe.html.twig', [
            'classements' => $classements,
            'rencontresGrouped' => $rencontresGrouped,
            'lienDivision' => $lienDivision,
        ]);
    }

    public function equipeStatsAction(string $lienDivision, string $equipeNom, StatsBuilder $statsBuilder)
    {
        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('club.equipe.stats')
            ->setLabelParameters([
                '%name%' => $equipeNom,
            ]);

        $matchStats =  $statsBuilder->getStatsEquipe($lienDivision, $equipeNom);

        return $this->render('@AlFFTT/club/equipeStats.html.twig', [
            'matchStats' => $matchStats,
        ]);
    }

    public function rencontreAction(Request $request, CacheRetriever $cacheRetriever)
    {
        $lien = $request->get('lien');
        $forceReload = $request->get("reload") == "true";

        $clubEquipeA = $request->get('clubEquipeA');
        $clubEquipeB = $request->get('clubEquipeB');

        try{
            $rencontre = $cacheRetriever->getRencontreDetails($lien, $clubEquipeA, $clubEquipeB, $forceReload);
            if(!$rencontre){
                throw new InvalidLienRencontre("GEN");
            }
        }
        catch (InvalidLienRencontre $e){
            return $this->render('@AlFFTT/Equipes/rencontreNotFound.html.twig');
        }

        /** @var RencontreDetails $rencontre */

        $rencontreStats = RencontreStats::createFromRencontreDetails($rencontre);

        return $this->render('@AlFFTT/Equipes/rencontre.html.twig', [
            'rencontre' => $rencontre,
            'stats' => $rencontreStats,
        ]);
    }
}
