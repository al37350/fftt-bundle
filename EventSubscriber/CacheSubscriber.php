<?php
/**
 * Created by Antoine Lamirault.
 */

namespace Al\FFTTBundle\EventSubscriber;


use Al\FFTTBundle\Controller\ReloadCache;
use Al\FFTTBundle\Service\EquipeDatabase;
use Al\FFTTBundle\Service\JoueurDatabase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\Process\Process;

class CacheSubscriber implements EventSubscriberInterface
{

    private $equipeAPI;
    private $joueurAPI;
    private $container;
    /**
     * @var FilesystemCache
     */
    private $cache;

    public function __construct(EquipeDatabase $equipeAPI, JoueurDatabase $joueurAPI, Container $container, FilesystemCache $cache)
    {
        $this->equipeAPI = $equipeAPI;
        $this->joueurAPI = $joueurAPI;
        $this->container = $container;
        $this->cache = $cache;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();
        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof ReloadCache) {
            $key = "lastDatabaseUpdate";

            if(!$this->cache->has($key)) {
                $process = new Process("php ../bin/console fftt:database:load -e prod");
                $process->start(function ($type, $buffer) {
                    if ('err' === $type) {
                        echo 'ERR > '.$buffer;
                    }
                    else {
                        echo 'OUT > '.$buffer;
                    }
                });

                $this->cache->set($key, true);
                $this->container->get('logger')->addInfo("Rechargement asynchrone des datas");
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::CONTROLLER => 'onKernelController',
        );
    }
}