<?php
/**
 * Created by PhpStorm.
 * User: alamirault
 * Date: 01/12/18
 * Time: 12:03
 */

namespace Al\FFTTBundle\Service;


use Al\FFTTBundle\Entity\Joueur;
use Al\FFTTBundle\Entity\Partie;
use Al\FFTTBundle\Model\JoueurStats\MatchStats;
use Al\FFTTBundle\Model\JoueurStats\ProportionClassementStats;
use Doctrine\ORM\EntityManagerInterface;
use FFTTApi\FFTTApi;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\Cache\Simple\FilesystemCache;

class StatsBuilder
{
    /**
     * @var HighchartBuilder
     */
    private $highchartBuilder;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var FFTTApi
     */
    private $api;
    /**
     * @var FilesystemCache
     */
    private $cache;
    /**
     * @var CacheRetriever
     */
    private $cacheRetriever;


    /**
     * StatsBuilder constructor.
     * @param HighchartBuilder $highchartBuilder
     * @param EntityManagerInterface $entityManager
     * @param FFTTApi $api
     * @param FilesystemCache $cache
     * @param CacheRetriever $cacheRetriever
     */
    public function __construct(HighchartBuilder $highchartBuilder, EntityManagerInterface $entityManager, FFTTApi $api, FilesystemCache $cache, CacheRetriever $cacheRetriever)
    {
        $this->highchartBuilder = $highchartBuilder;
        $this->entityManager = $entityManager;
        $this->api = $api;
        $this->cache = $cache;
        $this->cacheRetriever = $cacheRetriever;
    }

    public function getEvolutionByDay(Joueur $joueur): Highchart
    {

        $datas = $this->entityManager->getRepository(Partie::class)->sumByDay($joueur);

        $points = [];
        $xAxis = [];
        $lastPoints = $joueur->getLatestClassement()->getPointsInitials();
        foreach ($datas as $data) {
            $lastPoints += intval($data['somme']);
            $points[] = $lastPoints;
            $xAxis[] = $data['gbd'] . "/" . $data['gbm'] . "/" . $data['gby'];
        }

        return $this->highchartBuilder->buildEvolution('container', $xAxis, $points);
    }

    public function getStatMatchs(string $joueurId): MatchStats
    {
        $parties = $this->api->getPartiesJoueurByLicence($joueurId);

        $totalMatch = count($parties);
        $totalVictoire = 0;
        $totalDefaite = 0;

        $totalPointsVictoire = 0;
        $totalPointsDefaite = 0;

        $proportionClassementStats = new ProportionClassementStats();

        foreach ($parties as $partie) {
            if ($partie->isIsVictoire()) {
                $totalVictoire++;
                $totalPointsVictoire += ($partie->getPointsObtenus() * $partie->getCoefficient());

                $proportionClassementStats->addVictoire($partie->getAdversaireClassement());
            } else {
                $totalDefaite++;
                $totalPointsDefaite += ($partie->getPointsObtenus() * $partie->getCoefficient());

                $proportionClassementStats->addDefaite($partie->getAdversaireClassement());
            }
        }

        return new MatchStats(
            $totalMatch,
            $totalMatch,
            $totalDefaite,
            $totalPointsDefaite,
            $totalVictoire,
            $totalPointsVictoire,
            $proportionClassementStats
        );
    }

    public function getStatsEquipe(string $lienDivision, string $equipeNom): \Al\FFTTBundle\Model\EquipeStats\MatchStats
    {
        $rencontres = $this->api->getRencontrePouleByLienDivision($lienDivision);
        $classements = $this->api->getClassementPouleByLienDivision($lienDivision);

        $rencontresDetails = [];
        foreach ($rencontres as $rencontre) {
            if ($rencontre->getNomEquipeA() === $equipeNom || $rencontre->getNomEquipeB() === $equipeNom) {
                if (!($rencontre->getScoreEquipeA() == 0 && $rencontre->getScoreEquipeB() == 0)) {
                    foreach ($classements as $equipePoule) {
                        if ($rencontre->getNomEquipeA() === $equipePoule->getNomEquipe()) {
                            $rencontre->setClubEquipeA($equipePoule->getNumero());
                        }
                        if ($rencontre->getNomEquipeB() === $equipePoule->getNomEquipe()) {
                            $rencontre->setClubEquipeB($equipePoule->getNumero());
                        }
                    }
                    $rencontreDetail = $this->cacheRetriever->getRencontreDetails($rencontre->getLien(), $rencontre->getClubEquipeA(), $rencontre->getClubEquipeB());
                    if($rencontreDetail){
                        $rencontresDetails[] = $rencontreDetail;
                    }
                }
            }
        }

        $matchStats = new \Al\FFTTBundle\Model\EquipeStats\MatchStats($equipeNom);
        $matchStats->addRencontres($rencontresDetails);

        return $matchStats;
    }
}