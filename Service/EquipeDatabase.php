<?php
/**
 * Created by Antoine Lamirault.
 */

namespace Al\FFTTBundle\Service;


use Al\FFTTBundle\Entity\Equipe;
use Doctrine\ORM\EntityManager;
use FFTTApi\Exception\NoFFTTResponseException;
use FFTTApi\FFTTApi;

class EquipeDatabase
{
    private $em;
    private $FFTTApi;

    public function __construct(EntityManager $entityManager, FFTTApi $FFTTApi)
    {
        $this->em = $entityManager;
        $this->FFTTApi = $FFTTApi;
    }

    public function storeAllEquipes(string $clubId){
        $equipeRepository = $this->em->getRepository(Equipe::class);

        try{
            /** @var \FFTTApi\Model\Equipe[] $ffttJoueurs */
            $ffttEquipes = $this->FFTTApi->getEquipesByClub($clubId, 'M');
            foreach ($ffttEquipes as $ffttEquipe){
                if(!$equipeRepository->findOneByLienDivision($ffttEquipe->getLienDivision())){
                    $equipe = new Equipe();
                    $equipe->setLibelle($ffttEquipe->getLibelle());
                    $equipe->setDivision($ffttEquipe->getDivision());
                    $equipe->setLienDivision($ffttEquipe->getLienDivision());
                    $this->em->persist($equipe);
                }
            }
            $this->em->flush();
        }
        catch (NoFFTTResponseException $e){}
    }
}