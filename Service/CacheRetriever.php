<?php
/**
 * Created by PhpStorm.
 * User: alamirault
 * Date: 10/12/18
 * Time: 20:11
 */

namespace Al\FFTTBundle\Service;


use FFTTApi\Exception\InvalidLienRencontre;
use FFTTApi\Exception\JoueurNotFound;
use FFTTApi\FFTTApi;
use FFTTApi\Model\Classement;
use FFTTApi\Model\JoueurDetails;
use FFTTApi\Model\Rencontre\RencontreDetails;
use FFTTApi\Service\PointCalculator;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Simple\FilesystemCache;

class CacheRetriever
{
    /**
     * @var FilesystemCache
     */
    private $cache;
    /**
     * @var FFTTApi
     */
    private $api;
    /**
     * @var PointCalculator
     */
    private $pointCalculator;
    /**
     * @var LoggerInterface
     */
    private $logger;


    /**
     * CacheRetriever constructor.
     * @param FilesystemCache $cache
     * @param FFTTApi $api
     * @param PointCalculator $pointCalculator
     * @param LoggerInterface $logger
     */
    public function __construct(FilesystemCache $cache, FFTTApi $api, PointCalculator $pointCalculator, LoggerInterface $logger)
    {
        $this->cache = $cache;
        $this->api = $api;
        $this->pointCalculator = $pointCalculator;
        $this->logger = $logger;
    }

    /**
     * @param string $lien
     * @param string $clubEquipeA
     * @param string $clubEquipeB
     * @param bool $forceReload
     * @return RencontreDetails|null
     * @throws \FFTTApi\Exception\InvalidLienRencontre
     */
    public function getRencontreDetails(string $lien, string $clubEquipeA, string $clubEquipeB, bool $forceReload = false)
    {
        $keyValue = 'rencontre-' . $lien;

        if (!$forceReload && $this->cache->has($keyValue)) {
            $rencontre = $this->cache->get($keyValue);
        } else {
            try{
                $rencontre = $this->api->getDetailsRencontreByLien($lien, $clubEquipeA, $clubEquipeB);
                $this->cache->set($keyValue, $rencontre);
            }
            catch (InvalidLienRencontre $exception){
                return null;
            }
        }

        return $rencontre;
    }

    public function getNotValidatedMatchs(string $joueurLic, bool $forceReload): array
    {
        $key = "notValidatedMatch-" . $joueurLic;

        if (!$forceReload and $this->cache->has($key)) {
            $parties = $this->cache->get($key);
        } else {
            $parties = [];
            try {
                $joueurPoints = $this->getClassementJoueurByLicence($joueurLic, $forceReload)->getPoints();
            } catch (JoueurNotFound $e) {
                $joueurPoints = 500;
            }

            foreach ($this->api->getUnvalidatedPartiesJoueurByLicence($joueurLic) as $partie) {

                $virtualPointWithoutCoeff = $partie->isVictoire() ?
                    $this->pointCalculator->getPointVictory($joueurPoints, $partie->getAdversaireClassement()
                    ) :
                    $this->pointCalculator->getPointDefeat($joueurPoints, $partie->getAdversaireClassement()
                    );

                $parties[] = [
                    "partie" => $partie,
                    "virtualPoint" => $virtualPointWithoutCoeff * $this->pointCalculator->getCoefficientOfEpreuve($partie->getEpreuve()),
                ];
            }

            $this->cache->set($key, $parties);
        }
        return $parties;
    }

    public function getClassementJoueurByLicence(string $joueurLic, bool $forceReload = false): Classement
    {
        $keyValue = 'joueur-' . $joueurLic;

        if (!$forceReload && $this->cache->has($keyValue)) {
            $joueurData = $this->cache->get($keyValue);
        } else {
            $joueurData = $this->api->getClassementJoueurByLicence($joueurLic);

            $this->cache->set($keyValue, $joueurData);
        }

        return $joueurData;
    }

    public function getVirtualPoints(string $id, bool $forceReload)
    {
        $key = "virtualPoints-" . $id;

        if ($forceReload && $this->cache->has($key)) {
            $virtualPoints = $this->cache->get($key);
        } else {
            try {
                $joueurData = $this->getClassementJoueurByLicence($id, $forceReload);
                $virtualPoints = $joueurData->getPoints() + $this->api->getVirtualPoints($id);
            } catch (JoueurNotFound $e) {
                $virtualPoints = "NOK";
            }
            $this->cache->set($key, $virtualPoints);
        }

        return $virtualPoints;
    }

    public function getJoueurDetailsByLicence(string $licence, bool $forceReload): JoueurDetails
    {
        $key = "joueurDetails-" . $licence;

        if ($forceReload && $this->cache->has($key)) {
            $joueurDetails = $this->cache->get($key);
        } else {
            $joueurDetails = $this->api->getJoueurDetailsByLicence($licence);
            $this->cache->set($key, $joueurDetails);
        }

        return $joueurDetails;
    }
}