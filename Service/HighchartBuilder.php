<?php
/**
 * Created by PhpStorm.
 * User: alamirault
 * Date: 01/11/18
 * Time: 19:29
 */

namespace Al\FFTTBundle\Service;

use Al\FFTTBundle\Model\JoueurStats\MatchStats;
use Ob\HighchartsBundle\Highcharts\Highchart;

class HighchartBuilder
{
    public function buildEvolution(string $containerName, array $xAxis, array $points): Highchart
    {
        $ob = new Highchart();
        $ob->chart->renderTo($containerName);
        $ob->title->text("Evolution des points par journée ( sans dérive )");
        $ob->xAxis->categories($xAxis);
        $ob->yAxis->title(['text' => "Points"]);
        $ob->credits->enabled(false);
        $ob->legend->enabled(false);
        $series = [
            ["name" => "Points", "data" => $points],
        ];
        $ob->series($series);

        return $ob;
    }

    public function buildHisto(string $containerName, array $xAxis, array $points): Highchart
    {
        $ob = new Highchart();
        $ob->chart->renderTo($containerName);
        $ob->title->text('');
        $ob->xAxis->categories($xAxis);
        $ob->yAxis->title(['text' => "Points"]);
        $ob->credits->enabled(false);
        $ob->legend->enabled(false);
        $series = [
            ["name" => "Points", "data" => $points],
        ];
        $ob->series($series);

        return $ob;
    }

    public function buildRepartition(string $containerName, MatchStats $matchStats): Highchart
    {
        $ob = new Highchart();
        $ob->chart->renderTo($containerName);
        $ob->chart->type('column');
        $ob->title->text('Victoires/defaites par classement');
        $ob->xAxis->categories($matchStats->getProportionClassementStats()->getClassements());
        $ob->xAxis->title(['text' => "Classement adversaire"]);
        $ob->yAxis->title(['text' => "Nombre de matchs"]);
        $ob->credits->enabled(false);
        $ob->legend->enabled(false);
        $series = [
            ["name" => "Defaites", "data" => $matchStats->getProportionClassementStats()->getDefaites(), "color" => "#ef3f1c"],
            ["name" => "Victoires", "data" => $matchStats->getProportionClassementStats()->getVictoires(), "color" => '#02ce16'],
        ];
        $ob->series($series);
        $ob->plotOptions->column([
            "stacking" => "normal",
        ]);


        return $ob;
    }
}