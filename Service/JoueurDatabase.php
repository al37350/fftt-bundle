<?php
/**
 * Created by Antoine Lamirault.
 */

namespace Al\FFTTBundle\Service;


use Al\FFTTBundle\Entity\Classement;
use Al\FFTTBundle\Entity\Historique;
use Al\FFTTBundle\Entity\Joueur;
use Al\FFTTBundle\Entity\Partie;
use Doctrine\ORM\EntityManager;
use FFTTApi\Exception\JoueurNotFound;
use FFTTApi\FFTTApi;

class JoueurDatabase
{

    private $em;
    private $FFTTApi;

    public function __construct(EntityManager $entityManager, FFTTApi $FFTTApi)
    {
        $this->em = $entityManager;
        $this->FFTTApi = $FFTTApi;
    }


    public function storeAllJoueurs(string $clubId){
        $joueurRepository = $this->em->getRepository(Joueur::class);

        /** @var \FFTTApi\Model\Joueur[] $ffttJoueurs */
        $ffttJoueurs = $this->FFTTApi->getJoueursByClub($clubId);
        foreach ($ffttJoueurs as $ffttJoueur){
            if(!$joueurRepository->findOneByLicence($ffttJoueur->getLicence())){
                $joueur = new Joueur();
                $joueur->setNom($ffttJoueur->getNom());
                $joueur->setPrenom($ffttJoueur->getPrenom());
                $joueur->setLicence($ffttJoueur->getLicence());
                $this->em->persist($joueur);
            }
        }
        $this->em->flush();
    }

    public function storeClassementMoisJoueurs(){
        foreach( $this->em->getRepository(Joueur::class)->findAll() as $joueur){
            $this->storeClassementMoisJoueur($joueur);
        }
    }

    public function storeClassementMoisJoueur(Joueur $joueur){
        try{
            $classement = $this->FFTTApi->getClassementJoueurByLicence($joueur->getLicence());
            $databaseClassement = $this->em->getRepository(Classement::class)->findByJoueurIdAndDate($joueur->getId(), new \DateTime());


            if(!$databaseClassement){
                $databaseClassement = new Classement(
                    $classement->getDate(),
                    $classement->getPoints(),
                    $classement->getAnciensPoints(),
                    $classement->getClassement(),
                    $classement->getRangNational(),
                    $classement->getRangRegional(),
                    $classement->getRangDepartemental(),
                    $classement->getPointsOfficiels(),
                    $classement->getPointsInitials()
                );
                $databaseClassement->setJoueur($joueur);
            }

            $this->em->persist($databaseClassement);
            $this->em->flush();
        }
        catch(JoueurNotFound $e){}
    }

    public function storeHistoriqueJoueurs(){
        foreach( $this->em->getRepository(Joueur::class)->findAll() as $joueur){
            $this->storeHistoriqueJoueur($joueur);
        }
    }

    public function storeHistoriqueJoueur(Joueur $joueur){
        $historiques=[];
        try{
            $historiques = $this->FFTTApi->getHistoriqueJoueurByLicence($joueur->getLicence());
        }
        catch(JoueurNotFound $e){}

        foreach ($historiques as $historique){
            $histo = $this->em->getRepository(Historique::class)->findOneBy([
               'anneeDebut' => $historique->getAnneeDebut(),
               'anneeFin' => $historique->getAnneeFin(),
               'phase' => $historique->getPhase(),
               'joueur' => $joueur,
            ]);
            if(!$histo){
                $databaseHistorique = new Historique();
                $databaseHistorique->setJoueur($joueur);
                $databaseHistorique->setAnneeDebut($historique->getAnneeDebut());
                $databaseHistorique->setAnneeFin($historique->getAnneeFin());
                $databaseHistorique->setPhase($historique->getPhase());
                $databaseHistorique->setPoints($historique->getPoints());
                $this->em->persist($databaseHistorique);
            }
        };
        $this->em->flush();
    }

    public function storePartiesJoueurs(){
        foreach( $this->em->getRepository(Joueur::class)->findAll() as $joueur){
            $this->storePartiesJoueur($joueur);
        }
    }

    public function storePartiesJoueur(Joueur $joueur){
        $ffttParties=[];
        try{
            $ffttParties = $this->FFTTApi->getPartiesJoueurByLicence($joueur->getLicence());
        }
        catch(JoueurNotFound $e){}

        foreach ($ffttParties as $ffttPartie){
            $databasePartie = $this->em->getRepository(Partie::class)->findOneBy([
                'journee' => $ffttPartie->getJournee(),
                'adversaireLicence' => $ffttPartie->getAdversaireLicence(),
                'joueur' => $joueur,
            ]);

            if(!$databasePartie) {
                $partie = new Partie();
                $partie->setJoueur($joueur);
                $partie->setIsVictoire($ffttPartie->isIsVictoire());
                $partie->setJournee($ffttPartie->getJournee());
                $partie->setDate($ffttPartie->getDate());
                $partie->setPointsObtenus($ffttPartie->getPointsObtenus());
                $partie->setCoefficent($ffttPartie->getCoefficient());
                $partie->setAdversaireLicence($ffttPartie->getAdversaireLicence());
                $partie->setAdversaireIsHomme($ffttPartie->isAdversaireIsHomme());
                $partie->setAdversaireNom($ffttPartie->getAdversaireNom());
                $partie->setAdversairePrenom($ffttPartie->getAdversairePrenom());
                $partie->setAdversaireClassement($ffttPartie->getAdversaireClassement());
                $this->em->persist($partie);
            }
        }
        $this->em->flush();
    }
}