<?php

namespace Al\FFTTBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('al_fftt');

        $rootNode
            ->children()
                ->arrayNode('api')
                    ->children()
                        ->scalarNode('id')->end()
                        ->scalarNode('password')->end()
                    ->end()
                ->end()
                ->scalarNode('club_id')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
