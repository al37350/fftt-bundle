<?php
/**
 * Created by Antoine Lamirault.
 */

namespace Al\FFTTBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LoadFFTTDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('fftt:database:load')
            ->setDescription('Load FFTT datas')
            ->addOption(
                'reset',
                null,
                InputOption::VALUE_NONE,
                'Voulez-vous réinitialiser la base avant les imports ?'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $api = $this->getContainer()->get('fftt.database.joueur');
        $apiEquipe = $this->getContainer()->get('fftt.database.equipe');

        if($input->getOption('reset')){
            $output->writeln([
                '<info>Supression de la base de données</info>'
            ]);

            $commands = array(
                'doctrine:database:drop' =>   array('command' => 'doctrine:database:drop', '--force'  => true),
                'doctrine:database:create' =>   array('command' => 'doctrine:database:create'),
                'doctrine:schema:create' =>   array('command' => 'doctrine:schema:create')
            );

            foreach ($commands as $commandName => $params ){
                $command = $this->getApplication()->find($commandName);
                $arrayInput = new ArrayInput($params);
                $command->run($arrayInput, $output);
            }
        }

        $output->writeln([
            '<info>Chargement des joueurs...</info>'
        ]);
        $api->storeAllJoueurs($this->getContainer()->getParameter('al_fftt.club_id'));

        $output->writeln([
            '<info>Chargement des classements...</info>'
        ]);
        $api->storeClassementMoisJoueurs();

        $output->writeln([
            '<info>Chargement des historiques...</info>'
        ]);
        $api->storeHistoriqueJoueurs();

        $output->writeln([
            '<info>Chargement des parties...</info>'
        ]);
        $api->storePartiesJoueurs();

        $output->writeln([
            '<info>Chargement des équipes...</info>'
        ]);
        $apiEquipe->storeAllEquipes($this->getContainer()->getParameter('al_fftt.club_id'));

        $output->writeln([
            '<info>Imports réussis</info>'
        ]);
    }

}