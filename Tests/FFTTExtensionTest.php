<?php
/**
 * Created by Antoine Lamirault.
 */

namespace Al\FFTTBundle\Tests;

use Al\FFTTBundle\Twig\FFTTExtension;
use PHPUnit\Framework\TestCase;

class FFTTExtensionTest extends TestCase
{
    public function testFormatPlayerName(){
        $extension = new FFTTExtension();
        $result = $extension->formatPlayerName(new \Twig_Environment(new \Twig_Loader_Array()), "lamirault", "antoine");

        $this->assertEquals("LAMIRAULT Antoine", $result);
    }
}