<?php
/**
 * Created by PhpStorm.
 * User: alamirault
 * Date: 09/12/18
 * Time: 14:04
 */

namespace Al\FFTTBundle\Model\EquipeStats;


use FFTTApi\Model\Rencontre\Joueur;
use FFTTApi\Model\Rencontre\Partie;
use FFTTApi\Model\Rencontre\Rencontre;
use FFTTApi\Model\Rencontre\RencontreDetails;

class MatchStats
{
    private $victoires;
    private $defaites;
    private $nulls;

    private $pointsGagne;
    private $pointsPerdu;

    /**
     * @var string
     */
    private $equipeNom;
    private $joueurRencontreStatsCollection;

    /**
     * MatchStats constructor.
     */
    public function __construct(string $equipeNom)
    {
        $this->victoires = 0;
        $this->defaites = 0;
        $this->nulls = 0;

        $this->pointsGagne = 0;
        $this->pointsPerdu = 0;

        $this->joueurRencontreStatsCollection = new JoueurRencontreStatsCollection();

        $this->equipeNom = $equipeNom;
    }

    /**
     * @param RencontreDetails[] $rencontres
     */
    public function addRencontres(array $rencontres)
    {
        $this->initializeJoueurs($rencontres);
        foreach ($rencontres as $rencontre) {
            $this->addRencontre($rencontre);
        }
    }

    private function initializeJoueurs(array $rencontres){
        foreach ($rencontres as $rencontre) {
            $joueurInA = $rencontre->getNomEquipeA() === $this->equipeNom;

            $joueurs = $joueurInA ? $rencontre->getJoueursA() : $rencontre->getJoueursB();
            foreach ($joueurs as $joueur) {
                $this->joueurRencontreStatsCollection->addJoueur($joueur);
            }
        }
    }

    /**
     * @param Joueur[] $joueurs
     * @param Partie[] $parties
     */
    private function addRencontreStats(RencontreDetails $rencontre, bool $joueurInA)
    {
        foreach ($rencontre->getParties() as $partie) {
            if($joueurInA){
                $victoire = $partie->getScoreA() > $partie->getScoreB();
                $this->joueurRencontreStatsCollection->initializeResult($rencontre->getNomEquipeB());
                $this->joueurRencontreStatsCollection->addResult($partie->getAdversaireA(), $rencontre->getNomEquipeB(), $victoire);
            }
            if(!$joueurInA){
                $victoire = $partie->getScoreB() > $partie->getScoreA();
                $this->joueurRencontreStatsCollection->initializeResult($rencontre->getNomEquipeA());
                $this->joueurRencontreStatsCollection->addResult($partie->getAdversaireB(), $rencontre->getNomEquipeA(), $victoire);
            }
        }
    }

    public function addRencontre(RencontreDetails $rencontre)
    {
        if ($rencontre->getNomEquipeA() === $this->equipeNom) {
            $this->addRencontrePoints($rencontre->getScoreEquipeA(), $rencontre->getScoreEquipeB());
            $this->addRencontreStats($rencontre, true);
        }
        if ($rencontre->getNomEquipeB() === $this->equipeNom) {
            $this->addRencontrePoints($rencontre->getScoreEquipeB(), $rencontre->getScoreEquipeA());
            $this->addRencontreStats($rencontre, false);
        }
    }

    private function addRencontrePoints(int $points, int $pointsAdverse)
    {
        $this->pointsGagne += $points;
        $this->pointsPerdu += $pointsAdverse;
        if (!($points === 0) || !($pointsAdverse === 0)) {
            if ($points === $pointsAdverse) {
                $this->nulls += 1;
            } elseif ($points > $pointsAdverse) {
                $this->victoires += 1;
            } else {
                $this->defaites += 1;
            }

        }
    }

    /**
     * @return int
     */
    public function getVictoires(): int
    {
        return $this->victoires;
    }

    /**
     * @return int
     */
    public function getDefaites(): int
    {
        return $this->defaites;
    }

    /**
     * @return int
     */
    public function getNulls(): int
    {
        return $this->nulls;
    }

    /**
     * @return int
     */
    public function getPointsGagne(): int
    {
        return $this->pointsGagne;
    }

    /**
     * @return int
     */
    public function getPointsPerdu(): int
    {
        return $this->pointsPerdu;
    }

    /**
     * @return string
     */
    public function getEquipeNom(): string
    {
        return $this->equipeNom;
    }

    /**
     * @return JoueurRencontreStatsCollection
     */
    public function getJoueurRencontreStatsCollection(): JoueurRencontreStatsCollection
    {
        return $this->joueurRencontreStatsCollection;
    }
}