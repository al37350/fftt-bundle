<?php
/**
 * Created by PhpStorm.
 * User: alamirault
 * Date: 09/12/18
 * Time: 14:48
 */

namespace Al\FFTTBundle\Model\EquipeStats;


use FFTTApi\Model\Rencontre\Joueur;

class JoueurRencontreStats
{
    private $joueur;

    private $results;

    /**
     * JoueurRencontreStats constructor.
     * @param $joueur
     */
    public function __construct(Joueur $joueur)
    {
        $this->joueur = $joueur;
        $this->results = [];
    }

    /**
     * @return Joueur
     */
    public function getJoueur(): Joueur
    {
        return $this->joueur;
    }

    /**
     * @return array
     */
    public function getResults(): array
    {
        return $this->results;
    }

    public function initializeResult(string $equipeAdverseNom)
    {
        if (!isset($this->results[$equipeAdverseNom])) {
            $this->results[$equipeAdverseNom] = [
                "victoire" => 0,
                "defaite" => 0,
            ];
        }
    }

    public function addResult(string $equipeAdverseNom, bool $isVictoire)
    {
        if ($isVictoire) {
            $this->results[$equipeAdverseNom]["victoire"] += 1;
        } else {
            $this->results[$equipeAdverseNom]["defaite"] += 1;

        }
    }

}