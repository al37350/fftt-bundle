<?php
/**
 * Created by PhpStorm.
 * User: alamirault
 * Date: 09/12/18
 * Time: 14:50
 */

namespace Al\FFTTBundle\Model\EquipeStats;


use FFTTApi\Model\Rencontre\Joueur;
use FFTTApi\Service\Utils;

class JoueurRencontreStatsCollection
{
    /**
     * @var JoueurRencontreStats[]
     */
    private $joueursRencontresStats;

    /**
     * JoueurRencontreStatsCollection constructor.
     */
    public function __construct()
    {
        $this->joueursRencontresStats = [];
    }

    /**
     * @return JoueurRencontreStats[]
     */
    public function getJoueursRencontresStats(): array
    {
        return $this->joueursRencontresStats;
    }

    public function addJoueur(Joueur $joueur){
        if(!$this->hasJoueur($joueur)){
            $this->joueursRencontresStats[] = new JoueurRencontreStats($joueur);
        }
    }

    public function hasJoueur(Joueur $joueur) : bool
    {
        foreach ($this->joueursRencontresStats as $joueurRencontreStats){
            if($joueurRencontreStats->getJoueur()->getLicence() === $joueur->getLicence()){
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $joueurNom
     * @return JoueurRencontreStats
     * @throws \Exception
     */
    public function getJoueur(string $joueurNom){
        list($nom, $prenom) = Utils::returnNomPrenom($joueurNom);

        foreach ($this->joueursRencontresStats as $joueurRencontreStats) {
            if ($joueurRencontreStats->getJoueur()->getNom() === $nom && $joueurRencontreStats->getJoueur()->getPrenom() === $prenom) {
                return $joueurRencontreStats;
            }
        }

        throw new \Exception(sprintf("Cannot find joueur %s in stats collection", $joueurNom));
    }

    public function addResult(string $joueurNom, string $equipeAdverseNom, bool $isVictoire)
    {
        if($joueurNom !== "Double"){
            $joueurRencontreStats = $this->getJoueur($joueurNom);
            $joueurRencontreStats->addResult($equipeAdverseNom, $isVictoire);
        }
    }

    public function initializeResult(string $equipeAdverseNom)
    {
        foreach ($this->joueursRencontresStats as $joueurRencontreStats) {
            $joueurRencontreStats->initializeResult($equipeAdverseNom);
        }
    }
}