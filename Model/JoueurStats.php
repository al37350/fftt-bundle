<?php
/**
 * Created by PhpStorm.
 * User: alamirault
 * Date: 30/10/18
 * Time: 21:29
 */

namespace Al\FFTTBundle\Model;


class JoueurStats
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $result;

    /**
     * @var int
     */
    private $win;

    /**
     * @var int
     */
    private $lose;

    /**
     * JoueurStats constructor.
     */
    public function __construct(string $name)
    {
        $this->lose = 0;
        $this->win = 0;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }


    /**
     * @return int
     */
    public function getWin(): int
    {
        return $this->win;
    }


    public function winMatch(){
        $this->win++;
        $this->result[] = true;
    }

    public function loseMatch(){
        $this->lose++;
        $this->result[] = false;
    }

    /**
     * @return int
     */
    public function getLose(): int
    {
        return $this->lose;
    }

    /**
     * @return array
     */
    public function getResult(): array
    {
        return $this->result;
    }
}