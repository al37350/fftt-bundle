<?php
/**
 * Created by PhpStorm.
 * User: alamirault
 * Date: 30/10/18
 * Time: 21:29
 */

namespace Al\FFTTBundle\Model;


use Doctrine\Common\Collections\ArrayCollection;

class EquipeStats
{
    /**
     * @var string
     */
    private $nomEquipe;

    /**
     * @var ArrayCollection|JoueurStats[]
     */
    private $joueurs;

    /**
     * EquipeStats constructor.
     * @param string $nomEquipe
     */
    public function __construct(string $nomEquipe)
    {
        $this->joueurs = new ArrayCollection();
        $this->nomEquipe = $nomEquipe;
    }

    /**
     * @return string
     */
    public function getNomEquipe(): string
    {
        return $this->nomEquipe;
    }


    /**
     * @return JoueurStats[]|ArrayCollection
     */
    public function getJoueurs()
    {
        return $this->joueurs;
    }

    /**
     * @param JoueurStats[]|ArrayCollection $joueurs
     */
    public function setJoueurs($joueurs)
    {
        $this->joueurs = $joueurs;
    }

    public function addJoueur(JoueurStats $joueurStats){
        $this->joueurs->add($joueurStats);
    }

    public function getJoueur(JoueurStats $joueurStats){
        $this->joueurs->add($joueurStats);
    }

    /**
     * @param string $playerName
     * @return JoueurStats|mnull
     */
    public function getJoueurByName(string $playerName){
        foreach ($this->joueurs as $joueurStats){
            if($joueurStats->getName() === $playerName){
                return $joueurStats;
            }
        }
        return null;
    }

    public function addResult(string $playerName, bool $win){
        $joueurStat = $this->getJoueurByName($playerName);

        if($joueurStat){
            if($win){
                $joueurStat->winMatch();
            }
            else{
                $joueurStat->loseMatch();
            }
        }
        else{
            $joueurStat = new JoueurStats($playerName);
            if($win){
                $joueurStat->winMatch();
            }
            else{
                $joueurStat->loseMatch();
            }
            $this->joueurs->add($joueurStat);
        }
    }
}