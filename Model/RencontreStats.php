<?php
/**
 * Created by PhpStorm.
 * User: alamirault
 * Date: 30/10/18
 * Time: 21:28
 */

namespace Al\FFTTBundle\Model;


use Doctrine\Common\Collections\ArrayCollection;
use FFTTApi\Model\Rencontre\RencontreDetails;

class RencontreStats
{

    /**
     * @var EquipeStats
     */
    private $equipeA;

    /**
     * @var EquipeStats
     */
    private $equipeB;

    /**
     * RencontreStats constructor.
     */
    public function __construct(string $equipeAName, string $equipeBName)
    {
        $this->equipeA = new EquipeStats($equipeAName);
        $this->equipeB = new EquipeStats($equipeBName);
    }

    public static function createFromRencontreDetails(RencontreDetails $rencontre) : self{
        $rencontreStats = new RencontreStats($rencontre->getNomEquipeA(), $rencontre->getNomEquipeB());
        foreach ($rencontre->getParties() as $partie){
            $playerAWin = $partie->getScoreA() > $partie->getScoreB();

            if($playerAWin){
                $rencontreStats->addResultTeamA($partie->getAdversaireA(), true);
                $rencontreStats->addResultTeamB($partie->getAdversaireB(), false);
            }
            else{
                $rencontreStats->addResultTeamA($partie->getAdversaireA(), false);
                $rencontreStats->addResultTeamB($partie->getAdversaireB(), true);
            }

        }
        return $rencontreStats;
    }


    /**
     * @return EquipeStats
     */
    public function getEquipeA(): EquipeStats
    {
        return $this->equipeA;
    }

    /**
     * @param EquipeStats $equipeA
     */
    public function setEquipeA(EquipeStats $equipeA)
    {
        $this->equipeA = $equipeA;
    }

    /**
     * @return EquipeStats
     */
    public function getEquipeB(): EquipeStats
    {
        return $this->equipeB;
    }

    /**
     * @param EquipeStats $equipeB
     */
    public function setEquipeB(EquipeStats $equipeB)
    {
        $this->equipeB = $equipeB;
    }

    public function addResultTeamA(string $playerName, bool $win){
        $this->equipeA->addResult($playerName, $win);
    }

    public function addResultTeamB(string $playerName, bool $win){
        $this->equipeB->addResult($playerName, $win);
    }

}