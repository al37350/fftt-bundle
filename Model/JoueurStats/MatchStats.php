<?php
/**
 * Created by PhpStorm.
 * User: alamirault
 * Date: 01/12/18
 * Time: 15:35
 */

namespace Al\FFTTBundle\Model\JoueurStats;


class MatchStats
{
    /**
     * @var int
     */
    private $totalStats;

    /**
     * @var int
     */
    private $totalDefaite;

    /**
     * @var int
     */
    private $totalVictoire;

    /**
     * @var ProportionClassementStats
     */
    private $proportionClassementStats;
    /**
     * @var int
     */
    private $totalPointsVictoire;
    /**
     * @var int
     */
    private $totalPointsDefaite;
    /**
     * @var int
     */
    private $totalMatch;

    /**
     * MatchStats constructor.
     * @param int $totalStats
     * @param int $totalMatch
     * @param int $totalDefaite
     * @param int $totalPointsDefaite
     * @param int $totalVictoire
     * @param int $totalPointsVictoire
     * @param ProportionClassementStats $proportionClassementStats
     */
    public function __construct(int $totalStats, int $totalMatch, int $totalDefaite, int $totalPointsDefaite,  int $totalVictoire, int $totalPointsVictoire, ProportionClassementStats $proportionClassementStats)
    {
        $this->totalStats = $totalStats;
        $this->totalDefaite = $totalDefaite;
        $this->totalVictoire = $totalVictoire;
        $this->proportionClassementStats = $proportionClassementStats;
        $this->totalPointsVictoire = $totalPointsVictoire;
        $this->totalPointsDefaite = $totalPointsDefaite;
        $this->totalMatch = $totalMatch;
    }

    /**
     * @return int
     */
    public function getTotalStats(): int
    {
        return $this->totalStats;
    }

    /**
     * @return float
     */
    public function getMoyenneParMatch(): float
    {
        return ($this->totalPointsVictoire + $this->totalPointsDefaite) / $this->totalMatch;
    }

    /**
     * @return int
     */
    public function getTotalDefaite(): int
    {
        return $this->totalDefaite;
    }

    /**
     * @return float
     */
    public function getMoyenneParMatchDefaite(): float
    {
        return $this->totalPointsDefaite / $this->totalDefaite;
    }

    /**
     * @return int
     */
    public function getTotalVictoire(): int
    {
        return $this->totalVictoire;
    }

    /**
     * @return float
     */
    public function getMoyenneParMatchVictoire(): float
    {
        return $this->totalPointsVictoire / $this->totalVictoire;
    }

    /**
     * @return ProportionClassementStats
     */
    public function getProportionClassementStats(): ProportionClassementStats
    {
        return $this->proportionClassementStats;
    }

    /**
     * @return int
     */
    public function getTotalPointsVictoire(): int
    {
        return $this->totalPointsVictoire;
    }

    /**
     * @return int
     */
    public function getTotalPointsDefaite(): int
    {
        return $this->totalPointsDefaite;
    }

    /**
     * @return int
     */
    public function getTotalMatch(): int
    {
        return $this->totalMatch;
    }
}