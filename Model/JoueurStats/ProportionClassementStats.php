<?php
/**
 * Created by PhpStorm.
 * User: alamirault
 * Date: 01/12/18
 * Time: 17:43
 */

namespace Al\FFTTBundle\Model\JoueurStats;


class ProportionClassementStats
{
    private $matchsParClasements;

    /**
     * ProportionClassementStats constructor.
     */
    public function __construct()
    {
        $this->matchsParClasements = [];
    }

    public function addVictoire(int $classement){
        if(isset($this->matchsParClasements[$classement])){
            $this->matchsParClasements[$classement]["victoires"] = $this->matchsParClasements[$classement]["victoires"] + 1;
        }
        else{
            $this->matchsParClasements[$classement] = [
                "victoires" => 1,
                "defaites" => 0
            ];
        }

        ksort($this->matchsParClasements);
    }


    public function addDefaite(int $classement){
        if(isset($this->matchsParClasements[$classement])){
            $this->matchsParClasements[$classement]["defaites"] = $this->matchsParClasements[$classement]["defaites"] + 1;
        }
        else{
            $this->matchsParClasements[$classement] = [
                "victoires" => 0,
                "defaites" => 1
            ];
        }
        ksort($this->matchsParClasements);
    }

    public function getVictoires() : array
    {
        $result = [];
        foreach ($this->matchsParClasements as $value){
            $result[] = $value["victoires"];
        }
        return $result;
    }

    public function getDefaites() : array
    {
        $result = [];
        foreach ($this->matchsParClasements as $value){
            $result[] = $value["defaites"];
        }
        return $result;
    }

    public function getClassements() : array
    {
        $result = [];
        foreach ($this->matchsParClasements as $key => $value){
            $result[] = $key;
        }
        return $result;
    }
}