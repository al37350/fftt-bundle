# README

Ce bundle est l'intégration de  [FFTT Api](https://gitlab.com/al37350/ffttAPI) pour votre projet Symfony 3.

## Installation

Installation en 4 étapes:

1. Télécharger AlFFTTBundle avec composer
2. Activer le Bundle
3. Configurer AlFFTTBundle
4. Charger les styles et les scripts
5. Mettre à jour le schema de la base de données
6. Charger les données (optionnel)

### Step 1: Télécharger AlFFTTBundle avec composer

Require `al37350/fftt-bundle` via composer:

```bash
php composer.phar require al37350/fftt-bundle

```

Composer va modifier le fichier composer.json et installer le bundle dans le dossier `vendor/al37350` de votre projet.

### Step 2: Activer le Bundle

Activer le Bundle dans le  kernel:

``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Al\FFTTBundle\AlFFTTBundle(),
        new Thormeier\BreadcrumbBundle\ThormeierBreadcrumbBundle(),
        new Ob\HighchartsBundle\ObHighchartsBundle(),
    );
}
```

### Step 3: Configurer AlFFTTBundle

Vous devez importer les configurations du bundle.

```yaml
# app/config/config.yml
imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }
    - { resource: "@AlFFTTBundle/Resources/config/config.yml" }
```

Vous devez maintenant configurer votre identifiant et mot de passe donnés par la FFTT.

```yaml
# app/config/config.yml
al_fftt:
    api:
        id: "votre_identifiant"
        password: "votre_password"
    club_id: "23370690"
```

Pour acceder à toutes le vues du bundle, il faut importer les routes dans routing.yml
```yaml
# app/config/routing.yml
fftt:
    resource: '@AlFFTTBundle/Resources/config/routing.yml'
    prefix: '/fftt'
```

Vous pouvez désormer accèder à la liste des joueurs : 
```
path/to/symfony/web/app_dev.php/fftt/joueur/list
```
### Step 4: Charger les styles et les scripts
```
    <script src="https://code.jquery.com/jquery-3.2.1.js"
            integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

```

```
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">
```

### Step 5: Mettre à jour le schema de la base de données

``` bash
$ php bin/console doctrine:schema:update --force
```


### Step 6: Charger les données

``` bash
$ php bin/console fftt:database:load
```

# Contribution

Si tu as des questions ou un feedback, ouvres une issue.
J'espère que ce bundle peut te servir, si c'est le cas n'hésite pas à le partager et à le recommander.


[@a_lamirault](https://twitter.com/a_lamirault)

# License

Ce bundle est sous licence MIT. [Voir la licence complète du bundle](/LICENSE)